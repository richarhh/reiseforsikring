const FakturaOversikt = (props) => {
  // Personens fakturaoversikt - Denne linken ville basert seg på id
  const fakturaOversikt = "http://www.fremtind.no";
  const documentLink = "http://www.fremtind.no";
  const person = props.person;
  return (
    <div className="faktura-oversikt">
      <h2>Du betaler {person.pris} kr/mnd </h2>
      <a href="http://www.fremtind.no">Fakturaoversikt&#x2192;</a>
      <table className="faktura-table">
        <tr>
          <td>
            <h3>Periode</h3>
          </td>
          <td>
            <p>
              {person.periodeStart} - {person.periodeSlutt}
            </p>
          </td>
        </tr>
        <tr>
          <td>
            <h3>Avtalenummer</h3>
          </td>
          <td>
            <p>
              <p>{person.avtaleNummer}</p>
            </p>
          </td>
        </tr>
        <tr>
          <td>
            <h3>Dokument</h3>
          </td>
          <td>
            <p>
              <a href="http://www.fremtind.no">Avtaledokument(PDF)&#x2197;</a>
            </p>
          </td>
        </tr>
      </table>
    </div>
  );
};

export default FakturaOversikt;
