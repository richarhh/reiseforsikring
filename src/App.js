import FakturaOversikt from "./FakturaOversikt";
import PageTitle from "./PageTitle";

function App() {
  // Variabel for å lagre personinfo
  const person = {
    id: 0,
    navn: "Per Andreas Hansen",
    pris: 249,
    avtaleNummer: "123456789",
    periodeStart: "01.01.2021",
    periodeSlutt: "01.01.2022",
  };

  return (
    <div className="App">
      <div className="text-container">
        <PageTitle person={person} />
        <FakturaOversikt person={person} />
      </div>
      <img src="reiseforsikring.png" alt="Reisende på Oslo S" />
    </div>
  );
}

export default App;
