const PageTitle = (props) => {
  const person = props.person;
  return (
    <div className="page-title">
      <h1>Reiseforsikring</h1>
      <h3>{person.navn}</h3>
    </div>
  );
};

export default PageTitle;
